function dtaval=func_dta(t)

dtaval = 0.25 * cos(2*pi*t) * 2*pi;