function rhs = func_rhs(Nodes,t)

% coordinates
x = Nodes(:,1); 
y = Nodes(:,2);
z = Nodes(:,3);

a = func_a(t);
dta = func_dta(t);

%% stationary sphere
% % right-hand side function such that the solution is u = exp(-t) x * y
% u = x .* y * exp(-t);
% rhs = -u ... % \mat u
%       + 0 ... % u (\nbg \cdot v)
%       + 6 * u ... % - \Laplace_{\Ga} u
%       - 1 * ( x.*y.*exp(-3*t).*(4*x.^4.*y.^2 + 4*x.^2.*y.^4 + 4*x.^2.*y.^2.*z.^2 - 8*x.^2.*y.^2 + x.^2 + y.^2) ); % -|\nbg u|^2 u


%% bouncing ellipsoid
x=Nodes(:,1);
y=Nodes(:,2);
z=Nodes(:,3);


dd0=1;
alph=exp(-1.*t);
dalph=-1.*alph;
aa=1+0.25.*sin(2*pi*t);
daa = 2*pi*0.25.*cos(2*pi*t);



xnenner=sqrt(x.^2./aa.^2 + y.^2 + z.^2);
xn1=2./aa.*x./xnenner./2;
rhs = x.*y.*(dalph+alph.*(daa./2./aa)+alph.*(daa./2./aa.*(1-xn1.^2)))...
      + dd0 .*alph.*x.*y.* ( 2./(aa.*xnenner.^2) + 1./xnenner.*(1./aa+1) .* ( (1./aa+ 1 + 1)./xnenner-1./xnenner.^3.*(x.^2./aa.^3 + y.^2 + z.^2) ) )...
      - 1 * ( (x.*y.*exp(-3.*t).*(x.^4 + a.^2.*y.^4 - 2.*a.*x.^2.*y.^2 + a.^2.*x.^2.*z.^2 + a.^2.*y.^2.*z.^2))./(x.^2 + a.^2.*y.^2 + a.^2.*z.^2) );
   
end