function sol = func_solution(Nodes,t)

% exact solution
sol = Nodes(:,1) .* Nodes(:,2) * exp(- t);