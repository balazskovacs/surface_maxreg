function surface_maxreg_tester
% 
% This code is the main driver file for the
% convergence tests for maximal regularity of evolving surface PDEs.
%
% Written for the paper: 
% B. Kovács and B. Li.
% Maximal regularity of backward difference time discretization 
% for evolving surface PDEs and its application to nonlinear problems.
% arXiv:2106.07951, 2022.
% 
% The code solves non-linear PDEs on evolving surfaces, it uses
% linear evolving surface finite element in space
% and a BDF method in time.
%
% Required inputs: 
%   - initial surface parametrisation
%   - initial values are based on normal and mean curvature
%   - time interval, stepsizes, meshes, etc.
% 
% Copyright (c) 2022, Balázs Kovács (balazs.kovacs@ur.de) 
% under the GNU General Public License, version 3.
% 
% If you use this code in any program, project, or publication, please acknowledge
% its authors by adding a reference to the above publication.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

format long;

k = 3;              % BDF order
T = 1;              % final time
% spatial refinements (0,...,9)
n_vect=(0:8);
% temporal refinements
tau_vect=.2*2.^(-0:-1:-5);

ODEsolver = 'explEuler';

%%%%%%%%%% 
% sphere (max n = 0:7)
surf_name = 'Sphere';
save_text = surf_name;

%% test loops
% loop over mesh sizes
for in = 1:length(n_vect)
    n = n_vect(in);
    
    %% initial data
    % loading linear mesh
%     Nodes = load(['../surfs/Sphere_nodes',num2str(n),'.txt']);
%     Elements = load(['../surfs/Sphere_elements',num2str(n),'.txt']);
    % loading quadratic mesh as linear
    Nodes = load(['../surfs/Sphere_nodes_p2_',num2str(n),'.txt']);
    Elements = load(['../surfs/Sphere_elements_plot_p2_',num2str(n),'.txt']);
    
    DOF = length(Nodes)
    
    %% loop over timestep sizes
    for jtau=1:length(tau_vect)
        tau=tau_vect(jtau)
        
        surface_maxreg_solver_test(n,Nodes,Elements,k,tau,T,ODEsolver , save_text );
	
    end
end



beep
