function convplots_fig
% Creates temporal convergence plots with the norms L^\infty(W^{1,\infty}) 
% for convergence test for maximal regularity of evolving surface PDEs.
% 
% Uses outputs of 'surface_maxreg_tester'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format long;
k = 3; % BDF order
T = 1; % final time

% spatial refinements
n_vect=(0:8);
% temporal refinements
tau_vect=.2*2.^(-0:-1:-5);


factor=10^(1-k);

%%
% figure

for i=1:length(n_vect)
    n=n_vect(i);
    
    % loading linear mesh
%     Nodes = load(['../surfs/Sphere_nodes',num2str(n),'.txt']);
    % loading quadratic mesh as linear
    Nodes = load(['../surfs/Sphere_nodes_p2_',num2str(n),'.txt']);
    dof = length(Nodes);

    DOF{i}=strcat(['dof ',num2str(dof)]);
    for j=1:length(tau_vect)
        tau=tau_vect(j);
            if exist(['results/errors_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.txt'], 'file')==2
                err=load( ['results/errors_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.txt'] );
                I=ismember(err(1,:),T);
                Points=(1:length(I));
                T_n = Points(I);
                error_Linfty(i,j) = max(err(2,1:T_n));
                error_W1infty(i,j) = max( err(2,1:T_n) + err(3,1:T_n) );
                error_L2(i,j) = max(err(4,1:T_n));
                error_H1(i,j) = max(err(5,1:T_n));
            else
                error_Linfty(i,j)=NaN;
                error_W1infty(i,j)=NaN;
                error_L2(i,j)=NaN;
                error_H1(i,j)=NaN;
            end
            
    end
end
DOF{i+1}=strcat(['$\mathcal{O}(\tau^',num2str(k),')$']);
% DOF{i+2}=strcat(['$\mathcal{O}(\tau^',num2str(k-1),')$']);

%% plot

% ylimits=[10^-5 10^0];
% f1=figure('position',[50 100 1200 500]);
% subplot(1,2,1)

ylimits=[10^-4 10^0];
f1=figure('position',[50 100 600 500]);


% loglog_conv(tau_vect,error_L2)
loglog_conv(tau_vect,error_W1infty)
hold on;
plot(tau_vect,tau_vect.^k/tau_vect(1).^k,'-. black','LineWidth',1);
% plot(tau_vect,tau_vect.^(k-1)/factor,': black','LineWidth',1);
% loglog_conv(tau_vect,Linfty_x,'red')
hold off;
% h1=title('$\|u-u_{h}^\ell\|_{L^\infty(L^2)}$');
h1=title('$\|u-u_{h}^\ell\|_{L^\infty(W^{1,\infty})}$');
set(h1,'Interpreter','latex','FontSize',18);
h1=xlabel('step size ($\tau$)');
set(h1,'Interpreter','latex','FontSize',16);
h1=ylabel('errors','Interpreter','latex');
set(h1,'Interpreter','latex','FontSize',16);
h2=legend(DOF,'Location','SouthEast','FontSize',10);
set(h2,'Interpreter','latex');
xlim([(1/1.2)*min(tau_vect) 1.2*max(tau_vect)])
ylim(ylimits)

% subplot(1,2,2)
% loglog_conv(tau_vect,error_H1)
% hold on;
% plot(tau_vect,tau_vect.^k/tau_vect(1).^k,'-. black','LineWidth',1);
% % plot(tau_vect,tau_vect.^(k-1)/factor,': black','LineWidth',1);
% % loglog_conv(tau_vect,Linfty_H,'red')
% hold off;
% h1=title('$\|u-u_{h}^\ell\|_{L^\infty(H^1)}$');
% set(h1,'Interpreter','latex','FontSize',18);
% h1=xlabel('step size ($\tau$)');
% set(h1,'Interpreter','latex','FontSize',16);
% % ylabel('error','Interpreter','latex')
% h2=legend(DOF,'Location','SouthEast','FontSize',10);
% set(h2,'Interpreter','latex');
% xlim([(1/1.2)*min(tau_vect) 1.2*max(tau_vect)])
% ylim(ylimits)
% 
% 
% saveas(f1,['figures/surface_maxreg_convplot_BDF',num2str(k),'_time_Linfty.fig']);
% saveas(f1,['figures/surface_maxreg_convplot_BDF',num2str(k),'_time_Linfty.eps'], 'epsc2');

saveas(f1,['figures/surface_maxreg_time_BDF',num2str(k),'.fig']);
saveas(f1,['figures/surface_maxreg_time_BDF',num2str(k),'.eps'], 'epsc2');

end

function loglog_conv(vect,err,varargin)

if ~isempty(varargin)
    color_text = varargin{1};
else
    color_text = 'black';
end

[n1 n2]=size(err);

symbols='sox.d+^*v><';
ms=[6 6 6 8 6 6 6 6 6 6 6];
gr=(linspace(.66,0,n1))';
colors=[gr gr gr];

for jj=1:n1
    loglog(vect,err(jj,:), ...
           'LineWidth',1,...
           'Marker',symbols(jj),...
           'MarkerSize',ms(jj),...
           'Color', color_text); %'Color', colors(jj,:));
    if jj==1
        hold on;
    end
end
hold off;

end