function nrm_out = comp_W1infty_seminorm(Nodes,Elements,u)
% Computing the W^{1,\infty} norm of the function u.
% 
% - two dimensional surfaces in three dimensions
% - linear surface finite elements
%
% Written in P2Q2Iso2D (see [BCH (2006)]; DOI: 10.1016/j.cam.2005.04.032) approach.
% Using cell structure conversion for sparse matrices;
% see: https://de.mathworks.com/matlabcentral/answers/203734-most-efficient-way-to-add-multiple-sparse-matrices-in-a-loop-in-matlab
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

format long;

% Dunavant quadrature of order 2 weights and the number of quadrature nodes
W=[0.166666666666666   0.166666666666666   0.166666666666666];
length_W = length(W);

%% load or compute and save the (precomputed) product tensors
if exist(['assembly_precomp_P1/precomp_values_P1_',num2str(length_W),'.mat'], 'file')==2
    precomp_values=load(['assembly_precomp_P1/precomp_values_P1_',num2str(length_W),'.mat']);
    F_eval = precomp_values.F_eval;
    M_eval = precomp_values.M_eval;
    grad_F_eval = precomp_values.grad_F_eval;
else
    disp(['Computing and storing precomputeable values (M=',num2str(length_W),').'])
    % Dunavant quadrature of order 2 nodes
    xieta = [0.666666666666667   0.166666666666667   0.166666666666667;
             0.166666666666667   0.166666666666667   0.666666666666667;
                             0                   0                   0];
    % precomputing data                     
    [F_eval,M_eval, grad_F_eval] = surface_assembly_precompute_for_P1(xieta,W);
    % saving data in the right directory, and creating directory if it not exists
    if exist('assembly_precomp_P1', 'dir') ~= 7
        mkdir('assembly_precomp_P1');
    end
    save(['assembly_precomp_P1/precomp_values_P1_',num2str(length_W),'.mat'],'F_eval','M_eval','grad_F_eval');
end

%% inicialization
% local gegrees of freedom, and full one vector
loc_dof = 3; % due to linear elements
e_loc_dof = ones(1,loc_dof);
% degree of freedom
dof = length(Nodes);


%% modifications of the loaded data
% !!! should be rewritten to best fit the code --> 
pa_xi_F_eval(1:3,1:length_W)=grad_F_eval(1,:,:);
pa_eta_F_eval(1:3,1:length_W)=grad_F_eval(2,:,:);
% <-- should be rewritten to best fit the code !!!

%% assembly
for i=1:size(Elements,1) % begin loop over elements
    %% element transofrmation and evaluations (combining the above two ommented blocks)
    % vertices of current element (as a 3 x 3 matrix)
    nodes_of_element = Nodes(Elements(i,:),:);
    
    % local nodal values of the input vector u
    u_loc = u(Elements(i,:),:);
    
    % the partial derivatives of the element transofrmation map
    pa_xi_Phi = nodes_of_element.' * pa_xi_F_eval;
    pa_eta_Phi = nodes_of_element.' * pa_eta_F_eval;
    % the third column of the Jacobian of the element transformation map
    % (the cross product of the two partial derivatives)
    D_3 = cross(pa_xi_Phi,pa_eta_Phi);
    % length (Euclidean norm) of the cross product
    nrm_cross = vecnorm(D_3,2,1);
    
    % the Jacobian of the element transformation map
    D(1:3,1,:) = pa_xi_Phi;
    D(1:3,2,:) = pa_eta_Phi;
    D(1:3,3,:) = D_3;
    
    %% stiffness matrix assembly
    % A|_{kj} = \int_{\Ga_h} \nb_{\Ga_h} \phi_j \cdot \nb_{\Ga_h} \phi_k
    % inverting the Jacobians, computing gradients of basis functions, and performing further calculations
    for ell = 1:length_W
        C_ell = inv(D(:,:,ell)); 
        
        nrm_loc(ell) = norm( C_ell.' * (grad_F_eval(:,:,ell) * u_loc) );
        
    end
    
    nrm(i,1) = max( nrm_loc );
    
    
end % end loop over elements

nrm_out = max( nrm );

end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [F_eval,M_eval,grad_F_eval] = surface_assembly_precompute_for_P1(xieta,W)
% This code precomputes and saves various evaluations of reference basis functions,
% and products of such evaluations in the quadrature nodes.
% 
% - standard reference element (with nodes (0,0), (1,0), (0,1))
% - standard linear finite elements
% 
% Written for P2Q2 approach.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% basis functions (or form functions) on the reference element
% Anonymous functions are used, for easy modification for other element types.
% F = (F_1 F_2 F_3)
F=@(xi,eta)[ 1-xi-eta xi eta];

% evaluating the basis functions on the reference element
F_eval=[];
for ind=1:length(W)
    F_eval=[F_eval ; F(xieta(1,ind),xieta(2,ind))];
end

% Mass matrix assembly: This code precomputes and saves 
% the 3 x 3 x M tensor containing the products of the
% basis functions on the reference element:
%   M_eval|_{kjl} = \vphi_k(xi_l) \vphi_j(xi_l)
M_eval=zeros(3,3,length(W));
for ell=1:length(W) 
    F_ell=F_eval(ell,:);
    M_eval(:,:,ell)=F_ell.'*F_ell;
end

% Stiffness matrix assembly: 
% Evaluating gradients of the basis functions in the quadrature nodes.
% grad F1
grad_F1=@(xi,eta) [-1;-1;0];
% grad F2
grad_F2=@(xi,eta) [1;0;0];
% grad F3
grad_F3=@(xi,eta) [0;1;0];

for ind=1:length(W)
    grad_F_eval(:,:,ind) = [ grad_F1(xieta(1,ind),xieta(2,ind)) grad_F2(xieta(1,ind),xieta(2,ind)) grad_F3(xieta(1,ind),xieta(2,ind)) ];
end
end