function surface_maxreg_solver_test(n,Nodes,Elements,k,tau,T,ODEsolver , save_text )
% 
% This code numerically solves evolving surface PDEs,
% for convergence test for maximal regularity of evolving surface PDEs.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

format long;    


%% save initial surface
% degrees of freedom
dof=length(Nodes);

Nodes_past = Nodes;

%% BDF coefficients
[delta,gamma]=BDF_tableau(k);

%% initial values
t=[];
for ind=0:k-1
    t_new = ind*tau;
    t = [t t_new];
    
    Nodes_new = [Nodes(:,1)*sqrt(func_a(t_new)) Nodes(:,2) Nodes(:,3)];
%     % evolving the surface using 'ODEsolver'
%     Nodes_new = evolve_surface(Nodes_past,t_new-tau,tau,ODEsolver);
% %     distance_func = Nodes_new(:,1).^2/func_a(t_new) + Nodes_new(:,2).^2 + Nodes_new(:,3).^2 - 1;
% %     if max( abs(distance_func) ) > 0.001
% %         disp(['SURFACE EVOLUTION ERROR!! (Error =',num2str(max( abs(distance_func) )),'.)' ])
% %     end
    
    % exact solution
    u_past(:,ind+1) = func_solution(Nodes_new,t_new);
    
    M = surface_assembly_P1_mass(Nodes_new,Elements);
    Mu_past(:,ind+1) = M * u_past(:,ind+1);
    
    % updating the surface
    Nodes_past = Nodes_new;
end


%% time integration
for steps = k:ceil(T/tau)
    % timestep
    t_new = t(end) + tau;
    
    %%%%%%%%
    Nodes_new = [Nodes(:,1)*sqrt(func_a(t_new)) Nodes(:,2) Nodes(:,3)];
    % evolving the surface using 'ODEsolver'
%     Nodes_new = evolve_surface(Nodes_past,t_new-tau,tau,ODEsolver);
%     distance_func = Nodes_new(:,1).^2/func_a(t_new) + Nodes_new(:,2).^2 + Nodes_new(:,3).^2 - 1;
% %     if max( abs(distance_func) ) > 0.01
% %         disp(['SURFACE EVOLUTION ERROR!! (Error =',num2str(max( abs(distance_func) )),'.)' ])
% %     end
    
    %%%%%%%%
    % extrapolation
    u_tilde = zeros(dof,1);
    for ind = 1:k
        u_tilde = u_tilde + gamma(k-ind+1) * u_past(:,end-k+ind);
    end
    
    %%%%%%%%
    % surface FEM assembly on the EXTRAPOLATED surface
    [M,A,f] = surface_assembly_P1_surface_maxreg(Nodes_new,Elements, u_tilde );
    
    %%%%%%%%
    % right-hand side function
    rhs = func_rhs(Nodes_new,t_new);
    
    %%%%%%%%
    % contributions from the past
    Mdu = 0 * u_tilde;
    for ind = 1:k
        Mdu = Mdu + delta(ind+1) * Mu_past(:,end-ind+1);
    end
    rho_u = tau * ( f + M * rhs ) - Mdu;
    
    %%%%%%%%
    % solving the linear systems
    u_new = ( delta(1) * M + tau*A ) \ rho_u;
    

    %%%%%%%%
    % updating the solutions, the time vector and the surface
    Nodes_past = Nodes_new;
    u_past(:,k+1) = u_new;
    u_past(:,1) = [];
    Mu_past(:,k+1) = M * u_new;
    Mu_past(:,1) = [];
    
    % new time
    t=[t t_new];
    
    %% errors
    % the exact solution
    u_exact = func_solution(Nodes_new,t_new);
    
    % computing errors
    err = u_new - u_exact;
    errors(1,1:steps+1)=t;
    errors(2,steps+1) = max( abs( err ) ); % L^\infty norm
    errors(3,steps+1) = comp_W1infty_seminorm(Nodes,Elements, err ); % W^{1,\infty} semi-norm
    errors(4,steps+1) = sqrt( err.' * M * err );
    errors(5,steps+1) = sqrt( err.' * (M+A) * err );
    
    
    % save errors as 'txt' file
    save(['results/errors_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.txt'], 'errors', '-ASCII');
    
end
% disp('')

