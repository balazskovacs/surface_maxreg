function convplots_fig_spatial
% Creates temporal convergence plots with the norms L^\infty(W^{1,\infty}) 
% for convergence test for maximal regularity of evolving surface PDEs.
% 
% Uses outputs of 'surface_maxreg_tester'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format long;
k=2;


T = 1;

poly_deg = 1;

% spatial refinements
n_vect=(0:8);
% temporal refinements
tau_vect=.2*2.^(-0:-1:-5);

%%
% figure

for j=1:length(tau_vect)
    tau=tau_vect(j);
    DOF{j}=strcat(['$\tau =',num2str(tau),'$']);
    for i=1:length(n_vect)
        n=n_vect(i);
            if exist(['results/errors_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.txt'], 'file')==2
                err=load( ['results/errors_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.txt'] );
                I=ismember(err(1,:),T);
                Points=(1:length(I));
                T_n = Points(I);
                error_Linfty(j,i) = max(err(2,1:T_n));
                error_W1infty(j,i) = max( err(2,1:T_n) + err(3,1:T_n) );
                error_L2(j,i) = max(err(4,1:T_n));
                error_H1(j,i) = max(err(5,1:T_n));
            else
                error_Linfty(j,i)=NaN;
                error_W1infty(j,i)=NaN;
                error_L2(j,i)=NaN;
                error_H1(j,i)=NaN;
            end
    end
end

for i=1:length(n_vect)
    n=n_vect(i);
    
    Nodes = load(['../surfs/Sphere_nodes',num2str(n),'.txt']);
    dof = length(Nodes);
    x(i) = 1 / sqrt(dof);  % !!!!!!!!!!!!!!!!!!
end
DOF{j+1}=strcat(['$\mathcal{O}(h^2)$']);

DOF_H1 = DOF;
DOF_H1{j+1}=strcat(['$\mathcal{O}(h)$']);

%% plot
factor=20;
ylimits=[10^-6 10^1];

% f1=figure('position',[50 100 1200 500]);
f1=figure('position',[50 100 600 500]);

% subplot(1,2,1)
% loglog_conv(x,error_L2)
% hold on; 
% loglog(x,x.^(poly_deg+1)*factor,'-. black','LineWidth',1); 
% % loglog_conv(x,Linfty_x,'red')
% hold off;
% h1=title('$\|u-u_{h}^\ell\|_{L^\infty(L^2)}$');
% set(h1,'Interpreter','latex','FontSize',18);
% h1=xlabel('mesh size ($h$)');
% set(h1,'Interpreter','latex','FontSize',16);
% h1=ylabel('errors','Interpreter','latex');
% set(h1,'Interpreter','latex','FontSize',16);
% h2=legend(DOF,'Location','SouthEast','FontSize',10);
% set(h2,'Interpreter','latex');
% xlim([(1/1.2)*min(x) 1.2*max(x)])
% ylim(ylimits)
% 
% subplot(1,2,2)
% loglog_conv(x,error_H1)
loglog_conv(x,error_W1infty)
hold on; 
loglog(x,x.^(poly_deg)*factor,'-. black','LineWidth',1);
hold off;
% h1=title('$\|u-u_{h}^\ell\|_{L^\infty(H^1)}$');
h1=title('$\|u-u_{h}^\ell\|_{L^\infty(W^{1,\infty})}$');
set(h1,'Interpreter','latex','FontSize',18);
h1=xlabel('mesh size ($h$)');
set(h1,'Interpreter','latex','FontSize',16);
% ylabel('error','Interpreter','latex')
h2=legend(DOF_H1,'Location','SouthEast','FontSize',10);
set(h2,'Interpreter','latex');
xlim([(1/1.2)*min(x) 1.2*max(x)])
ylim(ylimits)


% saveas(f1,['figures/surface_maxreg_convplot_space_Linfty.fig']);
% saveas(f1,['figures/surface_maxreg_convplot_space_Linfty.eps'], 'epsc2');

saveas(f1,['figures/surface_maxreg_space.fig']);
saveas(f1,['figures/surface_maxreg_space.eps'], 'epsc2');


function loglog_conv(vect,err,varargin)

if ~isempty(varargin)
    color_text = varargin{1};
else
    color_text = 'black';
end

[n1 n2]=size(err);

symbols='sox.d+^*v><';
ms=[6 6 6 8 6 6 6 6 6 6 6];
gr=(linspace(.66,0,n1))';
colors=[gr gr gr];

for jj=1:n1
    loglog(vect,err(jj,:), ...
           'LineWidth',1,...
           'Marker',symbols(jj),...
           'MarkerSize',ms(jj),...
           'Color', color_text); %'Color', colors(jj,:));
    if jj==1
        hold on;
    end
end
hold off;