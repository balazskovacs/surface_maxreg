function aval = func_a(t)

aval = 1 + 0.25 * sin(2*pi*t);